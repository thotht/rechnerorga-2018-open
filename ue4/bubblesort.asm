# Hauptprogramm

    .text
    .globl main
main:
    addiu  $sp,  $sp, -4      # save stack space for registers
    sw    $ra,  0($sp)       # save return address
    li $t4, 4                # mnenoic usage of $t4 as 4
    jal   readValues         # from console
    jal   sortValues
    jal   printValues        # to screen
    lw    $ra,  0($sp)       # restore return address
    addiu  $sp,  $sp, 4       # restore stack pointer
    jr    $ra

sortValues:
    la $t0, array   # Array base adress in $t0
jump_Ofor_S:
    li $t9, 0   # swap register, 0 swap has not occured, 1 swap has occured
    li $t8, 8   # Counter for inner for
jump_Ifor_S:
    mul $t1, $t8, $t4
    add $t1, $t1, $t0   # Lower address
    lw $t2, 0($t1)      # loading the given numbers
    lw $t3, 4($t1)
    sub $t5, $t2, $t3   # correct Order?
    blez $t5, correctOrder
    xor $t2, $t2, $t3 # No
    xor $t3, $t2, $t3
    xor $t2, $t2, $t3
    sw $t2, 0($t1)
    sw $t3, 4($t1)
    li $t9, 1   # Mark Occurence of swap
correctOrder:   # Yes
    addiu $t8, -1
    bgez $t8, jump_Ifor_S
    bnez $t9, jump_Ofor_S   # repeat if change has happened
    jr $ra

readValues:
    addiu $sp, $sp, -12 # Save Return address
    sw $v0, 8($sp)
    sw $ra, 4($sp)
    li $t0, 9 # Initialize Counter
jump_for_R:
    sw $t0, 0($sp) # Save Counter
    jal readInt
    lw $t0, 0($sp)
    mul $t1, $t0, $t4
    la $t8, array
    add $t8, $t1, $t8
    sw $v0, 0($t8) # Save Read Int in array
    addiu $t0, -1
    bgez $t0, jump_for_R
    lw $v0, 8($sp)
    lw $ra, 4($sp) # restore return address
    addiu    $sp, $sp, 12
    jr $ra

printValues:
    addiu $sp, $sp, -16 # Save Return address
    sw $a0, 12($sp)
    sw $v0, 8($sp)
    sw $ra, 4($sp)
    la $a0, print_ordered
    jal printString
    li $t0, 9 # Initialize Counter
jump_for_P:
    li $t7, 9
    sub $t7, $t7, $t0
    sw $t0, 0($sp) # Save Counter
    mul $t1, $t7, $t4
    la $t8, array
    add $t8, $t1, $t8
    lw $a0, 0($t8)
    jal printInt
    la $a0, space_insert
    jal printString
    lw $t0, 0($sp)
    addiu $t0, -1
    bgez $t0, jump_for_P
    lw $a0, 12($sp)
    lw $v0, 8($sp)
    lw $ra, 4($sp) # restore return address
    addiu $sp, $sp, 16
    jr $ra

printString:
    li    $v0, 4             # print a string
    syscall
    jr    $ra                # return to calling routine

printInt:
    li    $v0, 1             # print an integer
    syscall
    jr    $ra                # return to calling routine

readInt:
    li    $v0, 5             # read an integer
    syscall
    jr    $ra
.data
    array:     .space  40 #Reserves 40 Byte Space for Sorting (10 Ints)
    print_ordered: .asciiz "Ordered Numbers: "
    space_insert: .asciiz "  "
