.text
.globl main
main:
addiu 	$sp, $sp, -20
sw      $s1, 16($sp) # Saving of to-save Registers
sw      $s0, 12($sp)
sw    	$ra, 8($sp)
sw      $fp, 4($sp)
sw    	$v0, 0($sp)
addiu 	$fp, $sp, 8
la		$a0, text_1  #initial print
jal 	print_string
la		$a0, text_3
jal 	print_string
jal 	read_int  #read int
move	$s0, $v0
jal     algorithm  #call algorithm
move    $s1, $v0
la		$a0, text_4 #return print
jal 	print_string
move	$a0, $s1
jal		print_int
la		$a0, text_2
jal 	print_string
lw      $s1, 16($sp) # reset and return
lw      $s0, 12($sp)
lw    	$ra, 8($sp)
lw      $fp, 4($sp)
lw      $v0, 0($sp)
addiu 	$sp, $sp, 12
jr   	$ra

print_string:  #Print String
li    $v0, 4
syscall
jr    $ra

print_int:  #Print Int
li    $v0, 1
syscall
jr    $ra

read_int:  #Read Int
li    $v0, 5
syscall
jr    $ra

algorithm:
addiu    $sp, $sp, -12
sw      $s0, 0($sp)
sw      $s1, 4($sp)
sw      $ra, 8($sp)
li		$s1, 0
blez	$s0, jump_error
addi  	$t0, $s0, -10
bgtz    $t0, jump_error

jump_for:
add 	$s1, $s0, $s1
addi    $s0, $s0, -1
bgtz	$s0, jump_for
j		jump_return

jump_error:
la		$a0, text_5
jal 	print_string
li		$s1, -1
j       jump_return

jump_return:
move    $v0, $s1
lw      $s0, 0($sp)
lw      $s1, 4($sp)
lw      $ra, 8($sp)
addiu   $sp, $sp, 12
jr      $ra



.data
text_1: .asciiz "Rechnerorganisation Aufgabe:\n"
text_2: .asciiz "\nEnde!\n"
text_3: .asciiz "Geben Sie eine natuerliche Zahl zwischen 1 und 10 ein: "

text_4: .asciiz "Ergebnis = "
text_5: .asciiz "falsche Eingabe!\n"
