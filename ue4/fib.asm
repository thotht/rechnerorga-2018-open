    .text
    .globl main
main:
    addiu  $sp,  $sp, -4      # save stack space for registers
    sw    $ra,  0($sp)       # save return address
    la    $a0, text_read
    jal   printString
    jal   readInt
    move  $a0, $v0
    jal   fibonacci
    move  $t1, $v0
    move  $t0, $a0
    la    $a0, text_n
    jal   printString
    move  $a0, $t0
    jal   printInt
    la    $a0, text_fib
    jal   printString
    move  $a0, $t1
    jal   printInt
    lw    $ra,  0($sp)       # restore return address
    addiu  $sp,  $sp, 4       # restore stack pointer
    jr    $ra

fibonacci:
    addiu  $sp,  $sp, -12      # save stack space for registers
    sw    $ra,  0($sp)       # save return address
    addiu  $t9, $a0, -2
    blez  $t9, exit_returns1
    sw    $a0,  4($sp)
    addiu  $a0, -1
    jal   fibonacci
    sw    $v0, 8($sp)
    addiu  $a0, -1
    jal   fibonacci
    lw    $t0, 8($sp)
    add   $v0, $t0, $v0
    lw    $a0,  4($sp)
    j     exit
exit_returns1:
    li    $v0, 1
exit:
    lw    $ra,  0($sp)       # restore return address
    addiu  $sp,  $sp, 12       # restore stack pointer
    jr    $ra

readInt:
    li    $v0, 5             # read an integer
    syscall
    jr    $ra

printInt:
    li    $v0, 1             # print an integer
    syscall
    jr    $ra                # return to calling routine

printString:
    li    $v0, 4             # print a string
    syscall
    jr    $ra                # return to calling routine

.data
    text_n: .asciiz "N: "
    text_fib: .asciiz "\nFib(N): "
    text_read: .asciiz "\n>"
