     .text
     .globl main
main:
     addi    $sp, $sp, -16  # save space of stack for reg.
     sw      $v0, 0($sp)    # save $v0 on stack
     sw      $s0, 4($sp)    # save $s0 on stack
     sw      $s1, 8($sp)    # save $s1 on stack
     sw      $ra, 12($sp)   # save return address
     li      $v0, 4         # system call no. 4: print asciiz
     la      $a0, text1     # load address of text1
     syscall
     li      $v0, 4         # system call no. 4: print asciiz
     la      $a0, inptxt    # load address of inptxt
     syscall
     li      $v0, 5         # system call no. 5: console input
     syscall
     move    $s0, $v0       # move value N into $s0 register
     li      $t0, 1     #Set t0 to 1
     blez    $16, outofbounds         #is smaller 1?
     li      $t2, 12
     sub     $t1, $16, $t2
     bgtz    $t1, outofbounds    #Larger than 12?
     move    $t3, $s0
     li      $s1, 1       #Set s1 to 1 for multiplication
for:
     mul     $s1, $s1, $t3
     beq     $t3, $t0, return
     sub     $t3, $t3, $t0  #dekrementieren
     j       for
outofbounds:
     li     $s1, -1
return:
     li      $v0, 4         # system call no. 4: print asciiz
     la      $a0, end1      # load address of end1
     syscall
     li      $v0, 1         # system call no. 1: print integer
     move    $a0, $s0       # register $s0 has value N
     syscall
     li      $v0, 4         # system call no. 4: print asciiz
     la      $a0, nline
     syscall
     li      $v0, 4         # system call no. 4: print asciiz
     la      $a0, end2      # load address of end1
     syscall
     li      $v0, 1         # system call no. 1: print integer
     move    $a0, $s1       # register $s1 has value N!
     syscall
     li      $v0, 4         # system call no. 4: print asciiz
     la      $a0, nline
     syscall
     lw      $ra, 12($sp)   # restore return address
     lw      $s1, 8($sp)    # restore $s1 from stack
     lw      $s0, 4($sp)    # restore $s0 from stack
     lw      $v0, 0($sp)    # restore $v0 from stack
     addiu   $sp, $sp, 16   # restore stack pointer
     jr      $ra            # return to main program

       .data
text1: .asciiz "\nFakultaetsberechnung\n"
inptxt: .asciiz "Geben Sie eine natuerliche Zahl zwischen 1 und 12 ein: "
end1:  .asciiz "\nN  = "
end2:  .asciiz "N! = "
nline: .asciiz "\n"
