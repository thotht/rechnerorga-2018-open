.text
.globl main
main:
addiu $sp,  $sp, -4      # reserve stack space for 1 register
sw    $ra,  0($sp)       # save return address on stack
move  $fp,  $sp          # set $fp = $sp
li    $a0,  62           # set $a0 = '>' (ASCII char)
li    $v0,  11           # set $v0 = index of system call "print_char"
syscall                  # print on console: '>'
li    $v0,  5            # set $v0 = index of system call "read_int"
syscall                  # read integer value n from console
bgtz  $v0,  validn       # goto validn if n > 0
li    $v0,  1            # enforce n > 0
validn:
li    $t1,  4            # set $t1 = 4
mul   $t1,  $t1, $v0     # set $t1 = 4 * n
subu  $sp,  $sp, $t1     # reserve stack space for vector V
move  $a0,  $v0          # set $a0 = n
move  $a1,  $sp          # set $a1 = address of vector V
jal   algorithm          # call algorithm(n, V)
move  $t0,  $sp          # set $t0 = address of vector V
output:
lw    $a0,  0($t0)       # set $a0 = value of current vector element
li    $v0,  1            # set $v0 = index of system call "print_int"
syscall                  # print on console: value of current vector element
addiu $t0, $t0, 4        # set $t0 = $t0 + 4
beq   $t0, $fp, endln    # goto "endln" if end of vector reached
li    $a0,  44           # set $a0 = ',' (ASCII char)
li    $v0,  11           # set $v0 = index of system call "print_char"
syscall                  # print on console: ','
j output                 # continue output
endln:
li    $a0,  13           # set $a0 = CR (ASCII char)
li    $v0,  11           # set $v0 = index of system call "print_char"
syscall                  # print on console: CR
finish:                      # program execution finished
lw    $ra,  0($fp)       # restore return address from stack
addiu $sp,  $fp, 4       # release reserved stack space
jr    $ra                # return to operating system
algorithm:
li $t0, 0
li $t4, 4
addi $t9, $a0, -1        # t9 is n-1 for usage for loops
move $t2, $a1
init:
beq $t0, $a0, endinit
sw $0, ($t2)
addi $t2, $t2, 4        #Setting all to 0
addi $t0, $t0, 1
j init

endinit:
li $t0, 1   # $0 is i
li $t2, 2
div $t1, $a0, $t2 # t1 is m
mul $t2, $t1, $t4
add $t2, $t2, $a1
sw $t0, ($t2)     #($t2) is v(m)

outerfor:
sub $t8, $t9, $t0
blez $t8, endouter
lw $t3, ($t2)   # p is t3, p=v(m)
move $t5 $t1   # j is t5, j = m

innerfor:
mul $t6, $t5, $t4
add $t6, $t6, $a1   # ($t6) is v(j)
andi $t7, $t0, 1    # even/odd
beq $t7, 0, even
lw $t7, ($t6)
add $t3, $t3, $t7 # swapping p and v(j)
xor $t3, $t3, $t7
xor $t7, $t3, $t7
xor $t3, $t3, $t7
sw $t7, ($t6)
j iteratej

even:
beq $t9, $t5, iteratei
lw $t7, ($t6)   # v(j)
lw $t8, 4($t6)  # v(j+1)
add $t7, $t7, $t8 # v(j) = v(j)+v(j+1)
sw $t7, ($t6)
iteratej:
beq $t9, $t5, iteratei
addi $t5, $t5, 1
j innerfor

iteratei:
addi $t0, $t0, 1
j outerfor

endouter:
li $t0, 0   # t0 new j
endfor:
beq $t0, $t1, endOfAlgorithm
sub $t8, $t9, $t0
mul $t8, $t8, $t4
add $t8, $t8, $a1
mul $t7, $t0, $t4
add $t7, $t7, $a1
lw $t6, ($t8)
sw $t6, ($t7)
addi $t0, $t0, 1
j endfor
endOfAlgorithm:
jr    $ra                # return to main program
